import styled from 'styled-components';

const Card = styled.div `
    box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
    transition: 0.3s;
    width: 90%;
    border-radius: 5px;
    background-color: #242526;
    color: white;
    margin: 5%;
`;

const Cardcontainer = styled.div `
    display: flex;
    padding: 2px 16px;
    flex-direction: column;
`;

const Cardimage = styled.img `
    width: 80%;
    margin: 10%;
`;

const Carddate = styled.p `
    margin-left: auto;
`;

const Likecontainer = styled.div `
    display: flex;
    align-items: center;
    margin-bottom: 10px;
`;

const Thumb = styled.span `
    margin-left: auto;
`;

const Cardtext = styled.p `
    margin: 0 5px;
`;

function Picturecard(props) {
    return (
        <Card>
            <Cardimage src={props.picture} alt="img" />
            <Cardcontainer>
                <Carddate><b>{props.date}</b></Carddate>
                <Likecontainer>
                    <Thumb className="material-icons">thumb_up</Thumb>
                    <Cardtext>{props.like}</Cardtext>
                    <span className="material-icons">chat</span>
                    <Cardtext>{props.commentcount}</Cardtext>
                </Likecontainer>
            </Cardcontainer>
        </Card>
    )
}

export default Picturecard;