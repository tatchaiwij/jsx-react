import pic from './pikkanode.png';

import Navbar from './Navbar';
import Picturecard from './Picturecard'
import Footer from './Footer';

import styled from 'styled-components'

const current = new Date();
const date = `${current.getDate()}/${current.getMonth() + 1}/${current.getFullYear()}`;

const items = [
  {
    id: 0,
    postdate: date,
    like: 1,
    commentcount: 0,
    picture: pic
  },
  {
    id: 1,
    postdate: date,
    like: 2,
    commentcount: 1,
    picture: pic
  },
  {
    id: 2,
    postdate: date,
    like: 3,
    commentcount: 2,
    picture: pic
  },
  {
    id: 3,
    postdate: date,
    like: 4,
    commentcount: 3,
    picture: pic
  },
  {
    id: 4,
    postdate: date,
    like: 5,
    commentcount: 5,
    picture: pic
  },
  {
    id: 5,
    postdate: date,
    like: 6,
    commentcount: 6,
    picture: pic
  },
  {
    id: 6,
    postdate: date,
    like: 7,
    commentcount: 7,
    picture: pic
  },
  {
    id: 7,
    postdate: date,
    like: 8,
    commentcount: 8,
    picture: pic
  },
  {
    id: 8,
    postdate: date,
    like: 9,
    commentcount: 9,
    picture: pic
  },
  {
    id: 9,
    postdate: date,
    like: 10,
    commentcount: 10,
    picture: pic
  },
  {
    id: 10,
    postdate: date,
    like: 11,
    commentcount: 11,
    picture: pic
  },
  {
    id: 11,
    postdate: date,
    like: 12,
    commentcount: 12,
    picture: pic
  },
]

function App() {
  const itemschunk = chunkArrayInGroups(items, 4);
  const Table = styled.table`
    margin-top: 10px;
  `;

  const Body = styled.div`
  background-color: #161B22;
  `;
  return (
    <Body>
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"></link>
      <Navbar />
      <Table>
        <tbody>
          {itemschunk.map(chunk => {
            return (<tr key={Math.random()}>
              {chunk.map(item => {
                return <td key={item.id}><Picturecard date={item.postdate} like={item.like} commentcount={item.commentcount} picture={item.picture} /></td>
              })}
            </tr>)
          })}
        </tbody>
      </Table>
      <Footer />
    </Body>
  );
}

function chunkArrayInGroups(arr, size) {
  var myArray = [];
  for (var i = 0; i < arr.length; i += size) {
    myArray.push(arr.slice(i, i + size));
  }
  return myArray;
}

export default App;
