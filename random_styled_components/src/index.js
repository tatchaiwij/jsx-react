import React from 'react';
import ReactDOM from 'react-dom';
import RandomBox from './RandomBox';

ReactDOM.render(
  <React.StrictMode>
    <RandomBox />
  </React.StrictMode>,
  document.getElementById('root')
);
