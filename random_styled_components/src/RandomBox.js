import styled from 'styled-components'

const color = ['red', 'blue', 'green', 'purple', 'pink']

const Random = styled.div `
    padding: 10px;
    display:table;
    height: 50vw;
    width: 50vw;
    background-color: ${(props) => props.color};
`;

const Text = styled.span `
    color: white;
    display:table-cell;
    vertical-align:middle;
    text-align: center;
    font-size: ${(props) => props.size}px;
`;

function getRandomArbitrary(min, max) {
    return Math.random() * (max - min) + min;
}

function RandomBox() {
    const mycolor = color[Math.round(getRandomArbitrary(0,4))];
    const fontsize = getRandomArbitrary(20,40);
    return (
        <Random color={mycolor}>
            <Text size={fontsize}>Random Box</Text>
        </Random>
    )
}

export default RandomBox;