import './Footer.css';

function Footer() {
    return (
        <footer className="footer">
            <div className="container">
                <p className="footername">Tatchai Wijitwiengrat</p>
            </div>
        </footer>
    )
}

export default Footer;