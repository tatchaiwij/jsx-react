import './Picturecard.css';

function Picturecard(props) {
    return (
        <div className="card">
            <img className="cardimage" src={props.picture} alt="img" />
            <div className="cardcontainer">
                <p className="carddate"><b>{props.date}</b></p>
                <div className="likecontainer">
                    <span className="material-icons thumb">thumb_up</span>
                    <p className="cardtext">{props.like}</p>
                    <span className="material-icons">chat</span>
                    <p className="cardtext">{props.commentcount}</p>
                </div>
            </div>
        </div>
    )
}

export default Picturecard;