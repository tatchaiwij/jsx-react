import './Navbar.css';

import logo from './pikkanode.png';

function Navbar() {
    return (
        <header className="navbar">
            <div className="container">
                <img className="logo" src={logo} alt="logo" />
                <div className="container button-container">
                    <button className="header-button">Create pikka</button>
                    <button className="header-button">Sign-up</button>
                    <button className="header-button">Sign-in</button>
                    <button className="header-button">Sign-out</button>
                </div>
            </div>
        </header>
    )
}

export default Navbar;