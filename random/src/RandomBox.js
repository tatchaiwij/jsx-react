import './RandomBox.css';

const color = ['red', 'blue', 'green', 'purple', 'pink']

function getRandomArbitrary(min, max) {
    return Math.random() * (max - min) + min;
}

function RandomBox() {
    const mycolor = color[Math.round(getRandomArbitrary(0,4))];
    const fontsize = getRandomArbitrary(20,40);
    return (
        <div className="randombox" style={{backgroundColor: mycolor}}>
            <span className="text" style={{fontSize: fontsize}}>Random Box</span>
        </div>
    )
}

export default RandomBox;