import Content from './Content';
import Footer from './Footer';
import Header from './Header';
import React from 'react';

class App extends React.Component {
  render () {
    return<div>
      <Header />
      <Content />
      <Footer />
    </div>
  }
}

export default App;
